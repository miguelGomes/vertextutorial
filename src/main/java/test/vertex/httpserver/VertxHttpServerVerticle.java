package test.vertex.httpserver;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;

/**
 * Created by MiguelGomes on 7/16/16.
 */
public class VertxHttpServerVerticle extends AbstractVerticle {

    private static final int HTTP_SERVER_PORT = 9999;

    private HttpServer httpServer;

    @Override
    public void start() {
        httpServer = vertx.createHttpServer();

        httpServer.requestHandler(request -> {
            System.out.println("Request Received");

            if (request.method() == HttpMethod.POST) {
                handlePost(request);
            }

            HttpServerResponse response = request.response();

            String responseString = "Hello";

            response.setStatusCode(200);
            response.headers()
                    .add("Content-Length", String.valueOf(responseString.length()))
                    .add("Content-Type", "text/html")
                    .add("Cache-Control", "max-age=1000000")
            ;
            response.write(responseString);
            response.end();
        });

        httpServer.listen(HTTP_SERVER_PORT);

        System.out.println("[VertxHttpServerVerticle] HttpServer started in port : " + HTTP_SERVER_PORT);
    }

    private void handlePost(HttpServerRequest request) {
        Buffer fullRequest = Buffer.buffer();

        request.handler(buffer -> fullRequest.appendBuffer(buffer));

        request.endHandler(x -> System.out.println(fullRequest.toString()));
    }

}
