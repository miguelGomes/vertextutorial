package test.vertex.httpserver;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpClient;

/**
 * Created by MiguelGomes on 7/16/16.
 */
public class VertxHttpClientVerticle extends AbstractVerticle {

    @Override
    public void start() {

        HttpClient httpClient = vertx.createHttpClient();

        httpClient.getNow(80, "google.pt", "/", response -> {
            System.out.println("Response received");

            response.bodyHandler(buffer ->
                    System.out.println(buffer.toString())
            );
        });

    }

}
