package test.vertex;

import io.vertx.core.Vertx;
import test.vertex.eventbus.ConsumerEventBusVertical;
import test.vertex.eventbus.ProducerEventBusVertical;
import test.vertex.httpserver.VertxHttpClientVerticle;
import test.vertex.httpserver.VertxHttpServerVerticle;
import test.vertex.verticles.BasicVerticle;

/**
 * Created by MiguelGomes on 7/16/16.
 */
public class VertexApp {

    public static void main(String[] args) {
        httpClient();
    }

    public static void deployed() {
        Vertx vertx = Vertx.vertx();

        vertx.deployVerticle(new BasicVerticle(), x -> {
            System.out.println("Verticle Deployed");
        });
    }

    public static void eventBusExample() {

        Vertx vertx = Vertx.vertx();

        vertx.deployVerticle(new ConsumerEventBusVertical("1"));
        vertx.deployVerticle(new ConsumerEventBusVertical("2"));

        try {
            Thread.sleep(5000);
        } catch (Exception e){}

        vertx.deployVerticle(new ProducerEventBusVertical());

    }

    public static void startHttpServer() {
        Vertx vertx = Vertx.vertx();

        vertx.deployVerticle(new VertxHttpServerVerticle());
    }

    public static void httpClient() {
        Vertx vertex = Vertx.vertx();

        vertex.deployVerticle(new VertxHttpClientVerticle());
    }

}
