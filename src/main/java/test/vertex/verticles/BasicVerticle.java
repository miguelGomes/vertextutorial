package test.vertex.verticles;

import io.vertx.core.AbstractVerticle;

/**
 * Created by MiguelGomes on 7/16/16.
 */
public class BasicVerticle extends AbstractVerticle {

    @Override
    public void start() {
        System.out.println("BasicVerticle Started");

        vertx.deployVerticle(new FirstVerticle());
    }

    @Override
    public void stop() {
        System.out.println("BasicVerticle Stopped");
    }

}
