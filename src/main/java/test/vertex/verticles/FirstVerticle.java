package test.vertex.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;

/**
 * Created by MiguelGomes on 7/16/16.
 */
public class FirstVerticle extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) {
        System.out.println("FirstVerticle Started");
    }

    @Override
    public void stop(Future<Void> stopFuture) {
        System.out.println("FirstVerticle Stopped");
    }


}
