package test.vertex.eventbus;

import io.vertx.core.AbstractVerticle;

/**
 * Created by MiguelGomes on 7/16/16.
 */
public class ConsumerEventBusVertical extends AbstractVerticle {

    private String verticleName;

    public ConsumerEventBusVertical(String verticleName) {
        this.verticleName = verticleName;
    }

    @Override
    public void start() throws Exception {

        printVerticleMessage("Started");

        vertx.eventBus().consumer(EventBusEnum.TEST_EVENT_BUS.name(), message -> {
            printVerticleMessage("Message Received: " + message.body());
        });
    }

    private void printVerticleMessage(String message) {
        System.out.println("[ConsumerEventBusVertical - " + this.verticleName + "] " + message);
    }

    @Override
    public void stop() throws Exception {
    }

}
