package test.vertex.eventbus;

import io.vertx.core.AbstractVerticle;

/**
 * Created by MiguelGomes on 7/16/16.
 */
public class ProducerEventBusVertical extends AbstractVerticle {

    @Override
    public void start() {
        vertx.eventBus().publish(EventBusEnum.TEST_EVENT_BUS.name(), "Published String");
        vertx.eventBus().send(EventBusEnum.TEST_EVENT_BUS.name(), "Sent String");
    }

    @Override
    public void stop() {
    }

}
